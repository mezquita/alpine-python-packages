from python:alpine

#----------------------------------
# mezquita/alpine-python-packages
#----------------------------------

MAINTAINER Mezquita "km@mezquita.jp"
# docker run -it --name mypy  imagename sh

RUN apk update
# change timezone
RUN apk --update add tzdata && \
    cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime && \
    apk del tzdata && \
    rm -rf /var/cache/apk/*

RUN apk add --no-cache \
    musl-dev \
    gcc \
    libgcc \
    libxslt-dev \
    bzip2-dev \
    libbz2 \
    libxml2-dev \
    libxml2-utils \
    git

VOLUME /mnt/data

RUN pip install --upgrade pip

cmd python

